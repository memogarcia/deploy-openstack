# Deploy OpenStack

Focused on correctness of deployment with rollback!!

meaning that every build of your cloud will be tagged and updated as a whole, bring some freebsd to linux

    ./deploy-openstack.yml  -i hosts/lxc-hosts.ini -e @model/small.yml

## Rollback

When upgrading, it will clone|snapshot the container and upgrade, on fail it will revert back to the previous state.

## Technical debt

* [ ] LXD profiles should be decouple from ansible
* [ ] Static IP

## Things I would like to learn from this project

* OpenStack
* Networking
* Ceph
* Proper releasing process
  * jenkins
  * git
  * ansible
