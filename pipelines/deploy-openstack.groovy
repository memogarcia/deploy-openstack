pipeline {
    agent {
        label 'any'
    }
    stages {
        stage ('Pre-Clean'){
            steps {
                cleanWs()
            }
        }
        stage ('Checkout repos'){
            steps {
                dir ('openstack'){
                    git branch: '${RELEASE_BRANCH}', poll: false, url: 'https://gitlab.com/memogarcia/deploy-openstack.git'
                }
            }
        }
        stage ('Creating OpenStack snapshot'){
        }
        stage ('Pre-Flight tasks'){
        }
        stage ('Deploy OpenStack Controllers'){
            steps {
                dir ('openstack'){
                    sh "./deploy-openstack.yml  -e @model/${ANSIBLE_VARS}.yml"
                }
            }
        }
    }
}
